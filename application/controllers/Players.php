<?php
	
	class Players extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();
			$this->load->model('players_model');
			$this->load->helper('url_helper');
		}
		
		public function index() 
		{
			$this->load->view('create_players');
		}
		
		public function create_players()
		{
			$input_player = $this->input->post('players_name');
			if ( ! empty($input_player))
			{
				$this->players_model->add_players($input_player);
			}
			$this->get_players();
		}
		
		public function get_players()
		{
			$players_list = $this->players_model->get_players();
			echo json_encode($players_list);
		}
	}