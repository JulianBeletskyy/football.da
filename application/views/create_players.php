<!DOCTYPE html>
<html lang="en" ng-app="myApp">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="/assets/css/font-awesome.css" rel="stylesheet">
	<link href="/assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="/assets/css/style.css" rel="stylesheet">
	<title>Football</title>
</head>
<body ng-controller="footballCtrl">
	<div class="container-fluid header">
		<div class="container header">
			<button class="newGame btn btn-default modal-button">Нова гра</button>
			<button href="#modal" role="button" class="btn btn-default modal-button" data-toggle="modal">Додати гравця</button>
			<h1 class="zagolovok zagolovok-2">FOOTBALL</h1>
		</div>
	</div>
	<div class="field">
		<div class="container check-players">
			<fieldset class="check-field">
				<legend>Хто буде грати?</legend>
				<div class="printPlayers">
				</div>
			</fieldset>
		</div>
		<div class="container">
			<div class="knopka row">
				<input id="button" class="start btn btn-default" type="button" value="Крутить колесо фортуни" onclick='' />
			</div>
		</div>
	</div>
	<div class="container">
		<div id="komandy" class="row"></div>
	</div>
	<div class="container">
		<div id="tournament" class="row"></div>
	</div>
	
	<div id="modal" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<button type="button" class="close close-btn-modal" data-dismiss="modal" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i></button>
				<div class="modal-header">
					<h3 class="modal-h3">Додайте нового гравця</h3>
				</div>
				<div class="modal-body">
					<form>
						<label for="player_name">Введіть ім&prime;я</label>
						<input id="playerName" type="input" name="player_name" />
						<button id="addPlayer" class="btn btn-default add-btn-modal" type="button" >Додати гравця</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	<script src="/assets/js/jquery.js"></script>
	<script src="/assets/js/angular.js"></script>
	<script src="/assets/js/bootstrap.js"></script>
	<script src="/assets/js/player.js"></script>
	<script src="/assets/js/myApp.js"></script>
</body>
</html>