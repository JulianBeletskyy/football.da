<?php

	class Players_model extends CI_Model
	{
		public function __construct()
		{
			$this->load->database();
		}
		
		public function add_players($input_player)
		{
			$this->db->where('LOWER(players_name)', strtolower($input_player));
			$this->db->limit(1);
			$row = $this->db->get('players')->row_array();
			
			if ( empty($row)) 
			{
				$data_array = array('players_name' => $input_player,
									'players_default' => FALSE);
				return $this->db->insert('players', $data_array);
			}
			else 
			{
				return FALSE;
			}
			
		}
		
		public function get_players()
		{
			$items = array('default' => array(), 
						   'undefault' => array(),
						   'names' => array());
			$this->db->order_by('players_id', 'random');
			$result = $this->db->get('players')->result_array();
			
			foreach ($result as $row)
			{
				$items['names'][$row['players_id']] = $row['players_name'];
				if ( ! empty($row['players_default']))
				{
					$items['default'][] = $row;
				}
				else
				{
					$items['undefault'][] = $row;
				}
			}
			return $items;
		}
	}