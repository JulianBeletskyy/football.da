<?php

	class News_model extends CI_Model 
	{
		public function __construct()
		{
			$this->load->database();
		}
		
		public function get_news($slug = FALSE)
		{
			if ($slug === FALSE)
			{
				return $this->db->get('news')->result_array();
			}
			$this->db->where('slug', $slug);
			return $this->db->get('news')->row_array();
		}
		
		public function set_news($post)
		{
			print_r($post);
			return $this->db->insert('news', $post);
		}
	}