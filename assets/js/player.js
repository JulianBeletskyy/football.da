$(document).ready(function() {
	getPlayers();
	$('#addPlayer').on('click', function() {
		var playerName = $('#playerName').val();
		$.post('/players/create_players/', {'players_name': playerName}, function(items) {
			printPlayers(items);
		}, 'json');
	});
	$('#button').on('click', fortune);
	$('.newGame').on('click', reload);
});


function reload()
{
	window.location.reload();
}

function getPlayers() 
{
	$.post('/players/get_players/', {}, function(items) {
		printPlayers(items);
	}, 'json');
}

var names = {};
function printPlayers(items)
{
	names = items['names'];
	var content = '';
	for (var j in items['default'])
	{
		var player = items['default'][j];
		content += '<label class="col-sm-2 player-in-check btn btn-default">';
		content += '<input type="checkbox" value="' + player['players_id'] + '" class="checkbox-player" checked="checked" />';
		content += ' <span class="name-player">' + player['players_name'] + '</span>';
		content += '</label>';
	}
	
	for (var j in items['undefault'])
	{
		var player = items['undefault'][j];
		content += '<label class="col-sm-2 player-in-check btn btn-default">';
		content += '<input type="checkbox" value="' + player['players_id'] + '" class="checkbox-player" />';
		content += ' <span class="name-player">' + player['players_name'] + '</span>';
		content += '</label>';
	}

	$('.printPlayers').html(content);
	
}

var use_footballer = [];
var min = 0;

function fortune() 
{
	$('#komandy').html(' ');
	var list = playersID();
	var num_players_in_team = (list.length < 4) ? 1 : 2;
	var num_teams = Math.floor(list.length / num_players_in_team);
	var teams = {};
	
	for (var i = 0; i < num_teams; i++) 
	{
		teams[i] = {};
		teams[i].list_players = [];
		for (var k = 0; k < num_players_in_team; k++) 
		{
			var new_player = get_random_players(list.length - 1);
			teams[i].list_players.push(list[new_player]);
		}
		
		teams[i].color_team = (i == 0) ? 'yellow' : (i == 1 ? 'blue' : ( i == 2 ? 'red' : ( i == 3 ? 'purple' : 'green' )));
	}
	use_footballer = [];
	
	for (var i = 0; i < num_teams; i++)
	{
		var names_player = [];
		for (var f = 0; f < teams[i].list_players.length; f++)
		{
			var id = teams[i].list_players[f];
			names_player.push(names[id]);
		}
		$('#komandy').append('<div class="teamcontainer col-sm-' + (Math.ceil(12 / num_teams)) + '"><div class="divteam">'
													+ teams[i].color_team + '</div>' + '<div class="player"><p><i class="fa fa-shield fa-3x" aria-hidden="true"></i>&emsp;'
													+ names_player.join('</p><p><i class="fa fa-bolt fa-3x" aria-hidden="true"></i>&emsp;') + '</p></div></div>');
	}
	
	$('.field').slideUp();
	$('.modal-button').hide();
	$('.newGame').show();
	
	for (var i = num_teams / num_teams; i < num_teams; i++)
	{
		$('#tournament').append('<div class="result col-sm-' + (Math.ceil(12 / num_teams)) + '"><div class="first-result"><p>' + teams[0].color_team + 
		'</p><input type="input" /></div><div class="vs"><button >vs</button></div><div class="second-result"><p>' + teams[i].color_team + '</p><input type="input" /></div></div>');
	}
	
	for (var i = num_teams / num_teams + 1; i < num_teams; i++)
	{
		$('#tournament').append('<div class="result col-sm-' + (Math.ceil(12 / num_teams)) + '"><div class="first-result"><p>' + teams[1].color_team + 
		'</p><input type="input" /></div><div class="vs"><button>vs</button></div><div class="second-result"><p>' + teams[i].color_team + '</p><input type="input" /></div></div>');
	}
	
	for (var i = num_teams / num_teams + 2; i < num_teams; i++)
	{
		$('#tournament').append('<div class="result col-sm-' + (Math.ceil(12 / num_teams)) + '"><div class="first-result"><p>' + teams[2].color_team + 
		'</p><input type="input" /></div><div class="vs"><button>vs</button></div><div class="second-result"><p>' + teams[i].color_team + '</p><input type="input" /></div></div>');
	}
	
	for (var i = num_teams / num_teams + 3; i < num_teams; i++)
	{
		$('#tournament').append('<div class="result col-sm-' + (Math.ceil(12 / num_teams)) + '"><div class="first-result"><p>' + teams[3].color_team + 
		'</p><input type="input" /></div><div class="vs"><button>vs</button></div><div class="second-result"><p>' + teams[i].color_team + '</p><input type="input" /></div></div>');
	}
}

function playersID()
{
	var checkedPlayer = [];
	$('.checkbox-player:checked').each(function(){
		checkedPlayer.push($(this).val());
	});
	return checkedPlayer;
}

function get_random_players(max) 
{
	var random_f = get_random_int(min, max);
	var check = true;
	while (check) 
	{
		var use_check = false;
		for (var j = 0; j < use_footballer.length; j++)
		{
			if (random_f == use_footballer[j]) 
			{
				use_check = true;
			}
		}
		
		if ( ! use_check) 
		{
			check = false;
			use_footballer.push(random_f);
		}
		else 
		{
			random_f = get_random_int(min, max);
		}
	}
	
	return random_f;
}

function get_random_int(min, maxF) 
{
	return Math.floor(Math.random() * (maxF - min + 1)) + min;
}

$('#modal').on('shown.bs.modal', function(){
	$('#playerName').focus();
}
);
